# Feedback

Olá, aviso que esse projeto não está completo em relação a proposta, meu trabalho atual me exirge muita presença, e devo estar com constantes entregas, isso dificultou um pouco a entrega desse teste. Porém, boa parte da lógica no back foi construída e infelizmente não deu tempo de refletir todas as funções no Front.

Backend

Autenticação com Token JWT
Entity com integração Postgre
CRUD de Usuarios que são definidos (pascientes, médicos, enfermeiros) pela Role.
Criação e obtenção de categorias (Categorias da consulta, ex: Exame de Sangue, Raio X e etc)
Modificação de Roles
Proteção de rotas por de Roles
Sistema de Autorização

Frontend

Login com proteção de rotas (AuthGuard)
Mini menu com as opções disponíveis
Página com tabela de usuários com organizador ASC e DESC
Formulário de inclusão de usuário funcional