﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using health_api.Data;
using health_api.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace health_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExamsController : ControllerBase
    {
        private readonly DataContext _context;
        public ExamsController(DataContext context)
        {
            _context = context;
        }

        [HttpPost]
        [Route("{cpf}")]
        private async Task<ActionResult> createExam(string cpf, [FromBody] Exam exam)
        {
            var user = _context.Users.Where(u => u.CPF == cpf).FirstOrDefault();
            if (user == null)
                return NotFound(new { message = "Usuário não encontrado" });

            _context.Add<Exam>(exam);
            await _context.SaveChangesAsync();

            return Ok(exam);
        }
    }
}
