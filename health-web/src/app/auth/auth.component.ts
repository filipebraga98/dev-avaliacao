import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';
import { UserService } from 'src/services/user.service';

import { AuthModel } from 'src/models/auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  authForm: FormGroup;

  loading = false;
  errorMessage

  constructor(private router: Router, private fb: FormBuilder, private authService: AuthService) {}

  ngOnInit(): void {
    this.authForm = this.fb.group({
      cpf: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
  }

  login() {
    if (!this.loading) {
      this.loading = true;

      const {cpf, password} = this.authForm.value;
  
      var auth: AuthModel = new AuthModel();
      auth.cpf = cpf;
      auth.password = password;
  
      this.authService.authUser(auth).subscribe(response => {
        this.loading = false;
        localStorage.setItem('token', response.auth.token);
        this.router.navigate(['/home']);
      }, e => {
        this.loading = false;
        this.errorMessage = "Usuário ou senha inválidos"
      })
    }
  }
}
