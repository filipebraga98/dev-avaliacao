import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthComponent } from './auth/auth.component';
import { AuthGuard } from 'src/services/auth.guard';
import { UsersComponent } from './users/users.component';
import { ExamsComponent } from './exams/exams.component';


const routes: Routes = [
  { path: "", component: AuthComponent },
  { path: "home", component: HomeComponent, canActivate: [AuthGuard] },
  { path: "users", component: UsersComponent, canActivate: [AuthGuard] },
  { path: "exams", component: ExamsComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
