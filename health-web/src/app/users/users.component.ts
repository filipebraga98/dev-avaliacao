import {Component, OnInit, ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

import { UserModel, RoleModel } from 'src/models/user';
import { UserService } from 'src/services/user.service';
import { AddressModel } from 'src/models/address';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

const ELEMENT_DATA: UserModel[] = [];

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'cpf', 'role'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  createUserForm: FormGroup;
  userAddressForm: FormGroup;
  user: UserModel;
  userAddress: AddressModel;

  displayNewUser = false;
  smooth = false;

  errorMessage

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  constructor(private userService: UserService, private fb: FormBuilder) { 
    this.createUserForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      cpf: ['', [Validators.required]],
      bornDate: [''],
      cellphone: [''],
      sexo: ['M'],
      password: ['', [Validators.required]]
    })

    this.userAddressForm = this.fb.group({
      Street : [''],
      CEP : [''],
      Complement : [''],
      Number : [0],
      City : [''],
      Country : [''],
      UF : ['']
    })
  }

  toggleNewUserForm() {
    this.displayNewUser = !this.displayNewUser;
  }

  createAccount() {
    if (!this.smooth) {
      console.log(this.createUserForm)
  
      if (this.createUserForm.dirty && this.createUserForm.valid) {
        this.smooth = true;
        this.user = Object.assign(new UserModel(), this.user, this.createUserForm.value);
  
        console.log('Usuario', this.user);
        this.userService.createUser(this.user).subscribe(res => {
          console.log(res);
          this.smooth = false;
        }, e => {
          this.smooth = false;
          this.errorMessage = e.error.message;
        })
      }
    }
  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe(response => {
      this.dataSource = new MatTableDataSource(response);
      this.dataSource.sort = this.sort;
    })

    
  }

  getAddress() {
    console.log('irra')

    this.userService.getEndCep(this.createUserForm.get('CEP').value).subscribe(res => {

      console.log(res)

      this.createUserForm.get('CEP').setValue(res.cep)
      this.createUserForm.get('Street').setValue(res.logradouro)
      this.createUserForm.get('Complement').setValue(res.complemento)
      this.createUserForm.get('City').setValue(res.localidade)
      this.createUserForm.get('UF').setValue(res.uf)
      this.createUserForm.get('Country').setValue("Brasil")
      this.createUserForm.get('CEP').setValue(res.cep)
    })
  }

}
