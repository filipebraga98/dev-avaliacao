import { UserModel } from 'src/models/user';

export class AuthModel {
    cpf: string
    password: string
}

export class AuthResponseModel {
    auth: TokenResponse
    user: UserModel
}

export class TokenResponse {
    expires: string
    token: string
}

