export class AddressModel {
      Id: number
      Street : string
      CEP : string
      Complement : string
      Number : number
      City : string
      Country : string
      UF : string
}
