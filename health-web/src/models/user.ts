import { ExamsModel } from "./exams"
import { AddressModel } from "./address"

export class UserModel {
    id: string
    name: string
    email: string
    cpf: string
    bornDate: string
    cellphone: string
    idade: number
    sexo: string
    exams: ExamsModel[]
    addressId: number
    address: AddressModel
    active: boolean
    role: RoleModel
}

export class RoleModel {
    id: number
    name: string
    badge: string
}
