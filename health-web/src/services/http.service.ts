import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';


import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export abstract class HttpService {

  protected url: string = environment.apiUrl;

  constructor(public http: HttpClient, public baseUrl: string) { }

  protected GetHeaders() {
    return {
      headers: new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('Authorization', 'Bearer ' + localStorage.getItem("token") || '')
    }
  }

  post(model: any, urlExtra: string) : Observable<any> {
    urlExtra = urlExtra == null ? "" : urlExtra;
    var normalizedUrl = this.url + this.baseUrl + urlExtra

    var headers = this.GetHeaders();

    let response = this.http.post(normalizedUrl, model, headers).pipe(
      map(this.extractData),
      catchError(this.serviceError)
    )

    return response
  }

  get(urlExtra: string = "") : Observable<any> {
    urlExtra = urlExtra == null ? "" : urlExtra;
    var normalizedUrl = this.url + this.baseUrl + urlExtra

    var headers = this.GetHeaders();

    let response = this.http.get(normalizedUrl, headers).pipe(
      map(this.extractData),
      catchError(this.serviceError)
    )

    return response
  }

  protected extractData(response: any) {
    return response.data || response;
  }

  protected serviceError(response: Response | any) {
    let customError: string[] = [];

    console.error(response);

    if (response instanceof HttpErrorResponse) {
      if (response.statusText === "Unknown Error") {
        customError.push("Ocorreu um error desconhecido");
        response.error.errors = customError;
      }
    }

    //console.error(response);
    return throwError(response);
  }
}
