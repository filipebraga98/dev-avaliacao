import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { UserModel, RoleModel } from '../models/user';
import { AddressModel } from 'src/models/address';

@Injectable({
  providedIn: 'root'
})
export class UserService extends HttpService {

  constructor(public http: HttpClient) { 
    super(http, 'users')
  }

  createUser(user: UserModel) : Observable<UserModel> {
    var response = this.post(user, null);
    return response;
  }

  getUsers() : Observable<UserModel[]> {
    var response = this.get();
    return response;
  }

  getUserRoles() : Observable<RoleModel[]> {
    var response = this.get('/roles');
    return response;
  }

  getEndCep(cep) : Observable<any> {
    var response = this.http.get(`https://viacep.com.br/ws/${cep}/json/`);
    return response;
  }
}
