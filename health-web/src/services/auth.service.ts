import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthModel, AuthResponseModel } from '../models/auth';

import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends HttpService {

  constructor(public http: HttpClient) { 
    super(http, 'auth')
  }

  authUser(auth: AuthModel) : Observable<AuthResponseModel> {
    var response = this.post(auth, null);
    return response;
  }

}
